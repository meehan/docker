# Image providing AthAnalysis-21.2.X for SLC6/CentOS7.

# Make the base image configurable.
ARG BASEIMAGE=atlas/slc6-atlasos:latest

# Set up the SLC6 "ATLAS OS".
FROM ${BASEIMAGE}

# Argument(s) that can be passed when building the image.
ARG RELEASE=21.2.83
ARG PLATFORM=x86_64-slc6-gcc62-opt
ARG WORKDIR=/home/atlas

# Helper variable(s).
ARG ANALYSISRELEASE=AthAnalysis_${RELEASE}_${PLATFORM}

# Helper environment variables for the image.
ENV AtlasProject AthAnalysis
ENV AtlasVersion ${RELEASE}
ENV CMTCONFIG ${PLATFORM}

# Perform the installation as root.
USER root
WORKDIR /root

# 1. Install all "ayum packages"
# 2. Clean up to reduce the image size
RUN yum -y install libuuid-devel libjpeg-turbo libtiff ${ANALYSISRELEASE} && \
    yum clean all && rm -rf /opt/lcg/gcc/6.2.0 && \
    rm -rf /opt/lcg/gcc/8.2*

# Add the release setup script to the home directory, and its
# "usage instructions".
COPY release_setup.sh /
COPY motd /etc/

# Switch to ATLAS account for the rest of the image
# setup.
USER atlas
WORKDIR ${WORKDIR}

# Set up a soft link for release_setup.sh inside the /home/atlas directory.
RUN ln -s /release_setup.sh /home/atlas/release_setup.sh

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
